// Створіть клас Phone, який містить змінні number, model і weight.
// Створіть три екземпляри цього класу.
// Виведіть на консоль значення їх змінних.
// Додати в клас Phone методи:receiveCall, має один параметр - ім'я.
// Виводить на консоль повідомлення "Телефонує {name}".
// Метод getNumber повертає номер телефону.
//  Викликати ці методи кожного з об'єктів.


class Phone {
	constructor(number, model, weight, name) {
		this.number = number;
		this.model = model;
		this.weight = weight;
		this.name = name;
	}
	receiveCall() {
		console.log("Телефонує " + this.name)
	}
	getNumber() {
		console.log(this.number)
	}
}

const phoneFirst = new Phone(380991231233, "Iphone", 230, "Jon");
const phoneSecond = new Phone(380997897458, "Iphone", 320, "Gary");
const phoneThird = new Phone(380995285654, "Iphone", 296, "Irina")
console.log(phoneFirst, phoneSecond, phoneThird)

phoneFirst.receiveCall()

phoneSecond.getNumber()




// - Написати функцію filterBy(), яка прийматиме 2 аргументи.
//  Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані,
// які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.
// Тобто якщо передати масив ['hello', 'world', 23, '23', null],
//  і другим аргументом передати 'string', то функція поверне масив [23, null].


function filterBy(arr, type) {
	return arr.reduce((res, item) => {
		if (typeof item != type) {
			res.push(item);
		}
		return res
	},
		[]
	);
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'))
